Lista = []
cont = 0

while True:
    diccionario = dict()

    diccionario['Nombre'] = input("Ingrese el nombre del trabajador: ")
    diccionario['TL'] = int(input("Ingrese el tiempo laborado en dias: "))
    diccionario['SM'] = int(input("Ingrese su cantidad de salarios minimos mensuales: "))
    diccionario['HED'] = int(input("Ingrese la cantidad de horas extras diurnas: "))
    diccionario['HEN'] = int(input("Ingrese la cantidad de horas extras nocturnas: "))
    diccionario['HEDF'] = int(input("Ingrese la cantidad de horas extras diurnas de domingos y festivos: "))
    diccionario['HENDF'] = int(input("Ingrese la cantidad de horas extras nocturnas de domingos y festivos: "))


    def salario(**diccionario):
        return diccionario.get('SM') * 850000

    def horas_extra(**diccionario):
        HE = round((diccionario.get('SM') / 240) * (diccionario.get('HED') * 1.25), 2)
        return HE

    def horas_extra_n(**diccionario):
        HEN = round((diccionario.get('SM') / 240) * (diccionario.get('HEN') * 1.35), 2)
        return HEN

    def horas_extra_df(**diccionario):
        HEDF = round((diccionario.get('SM') / 240) * (diccionario.get('HEDF') * 1.75), 2)
        return HEDF

    def horas_extra_n_df(**diccionario):
        HENDF = round((diccionario.get('SM') / 240) * (diccionario.get('HENDF') * 2.5), 2)
        return HENDF

    def cesantias(**diccionario):
        Cesantias = round(((diccionario.get('SM') * diccionario.get('TL')) / 360), 2)
        return Cesantias

    def icesantias(**diccionario):
        ICesantias = round(((cesantias(**diccionario) * diccionario.get('TL') * 0.12) / 360), 2)
        return ICesantias

    def primas(**diccionario):
        Primas = ((diccionario.get('SM') * 180) / 360)
        return Primas

    def vacaciones(**diccionario):
        Vacaciones = round(((diccionario.get('SM') * diccionario.get('TL')) / 720))
        return Vacaciones

    diccionario.update({'SM': salario(**diccionario)})
    diccionario.update({'HED': horas_extra(**diccionario)})
    diccionario.update({'HEN': horas_extra_n(**diccionario)})
    diccionario.update({'HEDF': horas_extra_df(**diccionario)})
    diccionario.update({'HENDF': horas_extra_n_df(**diccionario)})

    diccionario.update({'Liquidacion': {'Cesantias': cesantias(**diccionario),
                                        'ICesantias': icesantias(**diccionario),
                                        'Primas': primas(**diccionario),
                                        'Vacaciones': vacaciones(**diccionario)
                                        }
                        })
    cont = cont + 1
    Lista.append(diccionario)

    while True:
        resp = (input("\nIngrese 's' si desea ingresar otro trabajador, caso contario 'n': "))

        if resp == 's' or resp == 'n':
            break

    if resp == 'n':
        break

print("\n")
for i in range(cont):
    print(Lista[i])